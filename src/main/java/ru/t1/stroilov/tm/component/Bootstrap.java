package ru.t1.stroilov.tm.component;

import ru.t1.stroilov.tm.api.ICommandRepository;
import ru.t1.stroilov.tm.api.ICommandController;
import ru.t1.stroilov.tm.api.ICommandService;
import ru.t1.stroilov.tm.api.IBootstrap;
import ru.t1.stroilov.tm.constant.AppConstant;
import ru.t1.stroilov.tm.constant.ArgumentConstant;
import ru.t1.stroilov.tm.repository.CommandRepository;
import ru.t1.stroilov.tm.controller.CommandController;
import ru.t1.stroilov.tm.service.CommandService;

import java.util.Scanner;

public class Bootstrap implements IBootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ICommandController commandController = new CommandController(commandService);

    @Override
    public void run(String... args) {
        commandController.showWelcome();
        runApplication(args);
        runInput();
    }

    private void runInput() {
        System.out.println("Please enter command: ");
        final Scanner scanner = new Scanner(System.in);

        while (!Thread.currentThread().isInterrupted()) {
            parseInputArgument(scanner.nextLine());
            System.out.println();
        }
    }

    private void runApplication(final String[] args) {
        if (args == null || args.length == 0) return;
        final String arg = args[0];
        switch (arg) {
            case ArgumentConstant.VERSION:
                commandController.showVersion();
                break;
            case ArgumentConstant.INFO:
                commandController.showDeveloperInfo();
                break;
            case ArgumentConstant.HELP:
                commandController.showHelp();
                break;
            case ArgumentConstant.COMMANDS:
                commandController.showCommands();
                break;
            case ArgumentConstant.ARGUMENTS:
                commandController.showArguments();
                break;
            default:
                commandController.showUnknownCommand(arg);
                break;
        }
        shutDownApplication();
    }

    private void parseInputArgument(final String input) {
        if (input == null || input.isEmpty()) return;
        switch (input) {
            case AppConstant.VERSION:
                commandController.showVersion();
                break;
            case AppConstant.INFO:
                commandController.showDeveloperInfo();
                break;
            case AppConstant.HELP:
                commandController.showHelp();
                break;
            case AppConstant.COMMANDS:
                commandController.showCommands();
                break;
            case AppConstant.ARGUMENTS:
                commandController.showArguments();
                break;
            case AppConstant.EXIT:
                shutDownApplication();
                break;
            default:
                commandController.showUnknownCommand(input);
                break;
        }
    }

    private void shutDownApplication() {
        System.exit(0);
    }

}