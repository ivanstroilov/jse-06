package ru.t1.stroilov.tm.controller;

import ru.t1.stroilov.tm.model.Command;
import ru.t1.stroilov.tm.service.CommandService;
import ru.t1.stroilov.tm.api.ICommandService;

public class CommandController implements ru.t1.stroilov.tm.api.ICommandController {

    private final ICommandService commandService;

    public CommandController(ICommandService commandService) {
        this.commandService = commandService;
    }

    @Override
    public void showWelcome() {
        System.out.println("*** WELCOME TO TASK MANAGER ***");
    }

    @Override
    public void showHelp() {
        System.out.println("[HELP]");
        for (final Command command : commandService.getTerminalCommands()) {
            System.out.println(command.toString());
        }
    }

    @Override
    public void showVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.6.0");
    }

    @Override
    public void showDeveloperInfo() {
        System.out.println("[DEVELOPER INFORMATION]");
        System.out.println("Stroilov Ivan");
        System.out.println("ivanstroilov@gmail.com");
    }

    @Override
    public void showCommands() {
        System.out.println("[COMMANDS]");
        for (final Command command : commandService.getTerminalCommands()) {
            final String name = command.getName();
            if (name == null || name.isEmpty()) continue;
            System.out.println(name);
        }
    }

    @Override
    public void showArguments() {
        System.out.println("[ARGUMENTS]");
        for (final Command command : commandService.getTerminalCommands()) {
            final String argument = command.getArgument();
            if (argument == null || argument.isEmpty()) continue;
            System.out.println(argument);
        }
    }

    @Override
    public void showUnknownCommand(final String arg) {
        System.out.printf("Command '%s' not supported.\n", arg);
        System.out.println();
    }

}
