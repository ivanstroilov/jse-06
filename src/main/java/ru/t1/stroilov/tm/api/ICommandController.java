package ru.t1.stroilov.tm.api;

public interface ICommandController {

    void showWelcome();

    void showHelp();

    void showVersion();

    void showDeveloperInfo();

    void showCommands();

    void showArguments();

    void showUnknownCommand(String arg);

}