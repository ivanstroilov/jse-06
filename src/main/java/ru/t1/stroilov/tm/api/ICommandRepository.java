package ru.t1.stroilov.tm.api;

import ru.t1.stroilov.tm.model.Command;

public interface ICommandRepository {

    Command[] getCommandsArray();

}