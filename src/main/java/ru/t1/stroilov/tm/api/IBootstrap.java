package ru.t1.stroilov.tm.api;

public interface IBootstrap {

    void run(String... args);

}