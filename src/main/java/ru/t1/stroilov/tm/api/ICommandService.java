package ru.t1.stroilov.tm.api;


import ru.t1.stroilov.tm.model.Command;
import ru.t1.stroilov.tm.service.CommandService;

public interface ICommandService {

    Command[] getTerminalCommands();

}